import React, { useState } from 'react';
import './App.css';

const App = () => {

  const [counter, setCounter] = useState(0);

  const incrementCounter = () => setCounter(counter + 1);
  const decrementCounter = () => setCounter(counter ? counter - 1 : 0);

  return (
    <div className="App">
      <p>First TDD APP</p>
      <hr />
      <h1>This is counter app</h1>

      <div id="counter-value">{counter}</div>
      <button id="increment-btn" onClick={incrementCounter}>Increment</button>
      <button id="decrement-btn" onClick={decrementCounter}>Decrement</button>

    </div>
  );
}

export default App;
