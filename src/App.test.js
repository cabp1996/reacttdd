import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import { shallow } from 'enzyme';


describe('Counter Testing', () => {

  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<App />);
    // wrapper = mount(<App />);  //si quisiera probar por ejemplo <App><Counter/></App>
  });

  test('render title of counter', () => {
    //shallow crea una instancia del componente, solo el componente, no los componentes hijos
    console.log(wrapper.debug())
    expect(wrapper.find('h1').text()).toContain('This is counter app')
  });

  test('should render a button with text `increment`', () => {
    expect(wrapper.find('#increment-btn').text()).toBe('Increment');
  });

  test('should render the initial value of state in a div', () => {
    expect(wrapper.find('#counter-value').text()).toBe("0");
  });

  test('should render the click event of increment button and increment counter value', () => {
    // wrapper.find('#increment-btn').simulate('click');
    wrapper.find('#increment-btn').prop('onClick')();
    expect(wrapper.find('#counter-value').text()).toBe('1');
  });

  test('should render the click event of decrement button and decrement counter value', () => {
    wrapper.find('#increment-btn').simulate('click');
    expect(wrapper.find('#counter-value').text()).toBe('1');

    wrapper.find('#decrement-btn').simulate('click');
    expect(wrapper.find('#counter-value').text()).toBe('0');
  });

  
  test('when counter value is 0 it shouldnt go less than that if decrement buttonm is clicked', () => {
    wrapper.find('#decrement-btn').simulate('click');
    wrapper.find('#decrement-btn').simulate('click');
    expect(wrapper.find('#counter-value').text()).toBe('0');
  });
});


